#!/usr/bin/env python
# coding: utf-8

# # Practice

# In[1]:


import nltk
nltk.download()


# In[2]:


from nltk.tokenize import sent_tokenize, word_tokenize


# In[3]:


text = "The clever boy was walking on the red carpet"
print(sent_tokenize(text))
print(sent_tokenize(text))


# In[4]:


from nltk.stem.porter import PorterStemmer
# Reduce words to their stems
words = ['Name', 'is', 'Siddhesh', 'book']
stemmed = [PorterStemmer().stem(w) for w in words]
print(stemmed)


# In[5]:


from nltk.stem.wordnet import WordNetLemmatizer
# Reduce words to their root form
lemmed = [WordNetLemmatizer().lemmatize(w) for w in words]
print(lemmed)


# In[6]:


text = "The clever boy was walking on the red carpet which is made up of plastic"
print(sent_tokenize(text))
print(sent_tokenize(text))


# In[7]:


import nltk
from nltk.corpus import stopwords
stop_words = set(stopwords.words('english'))


# In[8]:


#print(stop_words)


# In[9]:


tagged1 = nltk.pos_tag(stemmed)
tagged2 = nltk.pos_tag(lemmed)
print(tagged1)
print(len(tagged2[3]))


# In[10]:


nltk.help.upenn_tagset()


# In[11]:


text = word_tokenize("I am going to book a flight.")
nltk.pos_tag(text)


# In[12]:


import pandas as pd
import csv

POS = set()
records = []

def data_mining(filename, var):
    df = pd.read_csv(filename , encoding= 'unicode_escape')
    df.columns = [var]
    df = df.dropna()
    for i in range(0, len(df[var])):
        word = str(df[var][i])
        try:
            data = word.split(" ")[0]
            word = word.split("(")[1]
            word = word.split(")")[0]
            pos = word.split(" ")
            if(len(pos) <  4):
                continue
            data1 = ""
            for p in pos:
                if(p != '&' and p[len(p)-1] == '.' and p[0] != '&'):
                    POS.add(str(p))
                    data1 = data1 + p + " "
            lst = []
            lst.append(data)
            lst.append(data1[0:len(data1)-1])
            records.append(lst)
        except:
            pass
    
for i in range(65, 65+26):
    var = str(chr(i))
    sub = "a\\a"
    filename = r'C:\Users\SIDDHESH\Documents\AI\NLP\Dictionary' + sub[1:2] + var + '.csv'
    data_mining(filename,var)

fields = ["Words", "POS"]
filename = r'C:\Users\SIDDHESH\Documents\AI\NLP\raw_mined_dict.csv'
with open(filename, 'w') as csvfile: 
    csvwriter = csv.writer(csvfile)  
    csvwriter.writerow(fields) 
    csvwriter.writerows(records)
print(POS)


# # Hidden Markov Model (Part A)

# In[13]:


f = open(r"C:\Users\SIDDHESH\Documents\AI\NLP\database.txt", 'r')
data = f.read()
data = data.split('\n')
print(len(data))
i = 0
word_set = set()
pos_set = set()
map_list = []
while(i < len(data)):
    j = i + 1
    sentence = data[i].split(" ")
    pos = data[j].split(" ")
    for word in sentence:
        if(len(word) != 0):
            word = word.lower()
            word_set.add(word)
    for p in pos:
        if(len(p) != 0):
            p = p.lower()
            pos_set.add(p)
    for k in range(0,len(pos)):
        lst = []
        lst.append(sentence[k].lower())
        lst.append(pos[k].lower())
        map_list.append(lst)
    i = i + 2


# In[14]:


# to create emission probability
emission_prob = {}
for word in word_set:
    emission_prob[word] = {}
    for pos in pos_set:
        emission_prob[word][pos] = 0
count_pos = {}
for pos in pos_set:
    count_pos[pos] = 0
for ele in map_list:
    emission_prob[ele[0]][ele[1]] += 1
    count_pos[ele[1]] += 1
for key1, value1 in emission_prob.items():
    for key2, value2 in emission_prob[key1].items():
        emission_prob[key1][key2] = emission_prob[key1][key2]/count_pos[key2]

for key, value in emission_prob.items():
    print(key, "   ", value)


# In[15]:


#transitsion probability
start = '<s>'
end = '<E>'
set1 = pos_set
set1.add(start)
set2 = pos_set
set2.add(end)
transition_prob = {}
for ele1 in set1:
    transition_prob[ele1] = {}
    for ele2 in set2:
        transition_prob[ele1][ele2] = 0
f = open(r"C:\Users\SIDDHESH\Documents\AI\NLP\database.txt", 'r')
data = f.read()
data = data.split('\n')
i = 0
while(i < len(data)):
    j = i + 1
    pos = data[j].split(" ")
    for k in range(0, len(pos) + 1):
        m = k - 1
        if(m == -1):
            transition_prob[start][pos[k]] += 1
        elif(k == len(pos)):
            transition_prob[pos[m]][end] += 1
        else:
            transition_prob[pos[m]][pos[k]] += 1
    i = i + 2
for key1, value1 in transition_prob.items():
    sum = 0
    for key2, value2 in transition_prob[key1].items():
        sum = sum + transition_prob[key1][key2]
    if(sum == 0):
        continue
    for key2, value2 in transition_prob[key1].items():
        transition_prob[key1][key2] = transition_prob[key1][key2]/sum
print(transition_prob)


# In[16]:


#prediction
ip = "Spot will see Will"
words = ip.split(" ")
for i in range(0,len(words)):
    words[i] = words[i].lower()
super_list = []
for word in words:
    sub_list = set()
    for tpl in map_list:
        if(tpl[0] == word):
            sub_list.add(tpl[1])
    super_list.append(list(sub_list))
print(super_list)


# In[17]:


max_prob = 0
word_list = words
soln = ['nil']
def find_suitable_pos(index, super_list, word_list, curr_list, curr_prob, max_prob):
    if(max_prob > curr_prob):
        return
    if(index > len(super_list)):
        if(max_prob < curr_prob):
            max_prob = curr_prob
            soln[0] = curr_list
            return
        return
    if(index == len(super_list)):
        prob = transition_prob[curr_list[-1]][end]
        curr_prob = curr_prob*prob
        find_suitable_pos(index+1,super_list, word_list, curr_list, curr_prob, max_prob)
        return
    elif(index < len(super_list)):
        for i in range(0, len(super_list[index])):
            if(index == 0):
                tr_p = transition_prob[start][super_list[index][i]]
            else:
                tr_p = transition_prob[curr_list[-1]][super_list[index][i]]
            em_p = emission_prob[word_list[index]][super_list[index][i]]
            if(tr_p == 0 or em_p == 0):
                continue
            prob = em_p * tr_p
            curr_prob = curr_prob*prob
            new_list = []
            new_list = curr_list
            new_list.append(super_list[index][i])
            if(len(new_list) > len(word_list)):
                new_list.pop()
                return
            find_suitable_pos(index+1,super_list, word_list, new_list, curr_prob, max_prob)
        
            
curr_list = []
res_pos = []
find_suitable_pos(0, super_list, word_list, curr_list, 1, max_prob) 
soln = soln[0]
for i in range(0, len(soln)):
    print(word_list[i] , " ---> ", soln[i])


# # Part B

# In[31]:


import nltk
from nltk import chunk

from nltk.tokenize import PunktSentenceTokenizer

train_text = "Aman and sidhesh are working on NLP and both are third year students."
sample_text = "A good boy will book a car."

custom_sent_tokenizer = PunktSentenceTokenizer(train_text)

tokenized = custom_sent_tokenizer.tokenize(sample_text)
tokenized = sent_tokenize(train_text)

def process_content():
    try:
        for i in tokenized:
            words = nltk.word_tokenize(i)
            tagged = nltk.pos_tag(words)
            chunkGram = r"""Chunk: {<JJ>?<NN.*>?<VB.*>}"""
            chunkParser = nltk.RegexpParser(chunkGram)
            chunked = chunkParser.parse(tagged)
            print(chunked)
            chunked.draw()  
            return   

    except Exception as e:
        print(str(e))

process_content()


# In[36]:


pattern_list = []
for key1, value1 in transition_prob.items():
    for key2, value2 in transition_prob[key1].items():
        if(transition_prob[key1][key2] != 0):
            lst = []
            lst.append(transition_prob[key1][key2])
            lst.append(key1)
            lst.append(key2)
            pattern_list.append(lst)
pattern_list.sort(key = lambda pattern_list: pattern_list[1])
for i in pattern_list:
    print(i[1] , " ----> ", i[2])


# # Part C

# In[37]:


NERS = ['PERSON','NORP','FAC','ORG','GPE','LOC','PRODUCT','EVENT','WORK_OF_ART','LAW','LANGUAGE','DATE','TIME','PERCENT','MONEY','QUANTITY','ORDINAL','CARDINAL']


# In[1]:


import spacy


# In[8]:


import spacy.displacy as displacy
nlp = spacy.load("en_core_web_lg")
doc = nlp("Siddhesh and Aman are two boys working on NLP projects. We are working in Pune on the language of Katkari people.")


# In[9]:


print(doc)


# In[10]:


for  ent in doc.ents:
    print(ent.text, ent.start_char, ent.end_char, ent.label_)


# In[11]:


displacy.serve(doc, style = 'ent')


# In[ ]:





# In[ ]:




